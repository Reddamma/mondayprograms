package Collection;
//Write a Java program to insert an element into the array list at the first position.
	import java.util.*;
	  public class Example3 {
	  public static void main(String[] args) {
	  ArrayList<String> al  = new ArrayList<String>();
	  al .add("Red");
	  al .add("Green");
	  al .add("Orange");
	  al .add("White");
	  al .add("Black");
	  System.out.println(al);
	  al .add(0, "Pink");
	  al .add(5, "Yellow");
	 System.out.println(al);
	 }
	}

