package Collection;

//Write a Java program to iterate through all elements in a array list.
	import java.util.*;
	public class Example2 {
	  public static void main(String[] args) {
	
	 ArrayList<String> al = new ArrayList<String>();
	 al .add("Red");
	 al .add("Green");
	 al .add("Orange");
	 al .add("White");
	 al .add("Black");
     for (String element :al) {
	    System.out.println(element);
	    }
	 }
	}

